import * as tape from "tape";
import { State } from "./State";

interface ICounter {
  count: number;
}

interface ITestState {
  counter: ICounter;
}

const INITIAL_STATE: ITestState = {
  counter: {
    count: 0
  }
};

tape("State createStore, setState, updateState", (assert: tape.Test) => {
  const state = new State(INITIAL_STATE),
    counter = state.getStore("counter");

  let eventSetStateCalled = false,
    eventSetStateForCalled = false;

  const metas: string[] = [];

  state.on("set-state", () => {
    eventSetStateCalled = true;
  });

  state.on("set-state-for", (name, state, meta) => {
    eventSetStateForCalled = true;
    metas.push(meta);
    assert.equals(name, "counter");
  });

  counter.setState({ count: 1 }, "reset");
  counter.updateState(({ count }) => ({ count: count + 1 }), "incement");

  assert.deepEquals(metas, ["reset", "incement"]);

  assert.deepEquals(state.getStateFor("counter"), { count: 2 });
  assert.deepEquals(state.getState().toJSON(), { counter: { count: 2 } });
  assert.deepEquals(counter.getState(), { count: 2 });

  assert.equals(eventSetStateCalled, true);
  assert.equals(eventSetStateForCalled, true);

  assert.end();
});

tape(
  "State no emit createStore, setState, updateState",
  (assert: tape.Test) => {
    const state = new State(INITIAL_STATE),
      counter = state.getStore("counter");

    let eventSetStateCalled = false,
      eventSetStateForCalled = false;

    const reset = () => {
      eventSetStateCalled = false;
      eventSetStateForCalled = false;
    };

    state.on("set-state", () => {
      eventSetStateCalled = true;
    });

    state.on("set-state-for", name => {
      eventSetStateForCalled = true;
      assert.equals(name, "counter");
    });

    counter.setState({ count: 1 }, "reset");
    assert.deepEquals(counter.getState(), { count: 1 });

    assert.equals(eventSetStateCalled, true);
    assert.equals(eventSetStateForCalled, true);

    reset();

    counter.noEmitSetState({ count: 0 });
    assert.deepEquals(counter.getState(), { count: 0 });

    assert.equals(eventSetStateCalled, false);
    assert.equals(eventSetStateForCalled, false);

    assert.end();
  }
);

tape("State/Store toJSON fromJSON", (assert: tape.Test) => {
  const state = new State(INITIAL_STATE),
    counter = state.getStore("counter");

  assert.deepEquals(counter.toJSON(), { count: 0 });
  counter.setStateJSON({ count: 2 });
  assert.deepEquals(counter.toJSON(), { count: 2 });

  state.setStateJSON({
    counter: { count: 1 }
  });
  assert.deepEquals(state.toJSON(), {
    counter: { count: 1 }
  });

  assert.end();
});
